package test.mel;

import test.mel.oop.Bonus;
import test.mel.oop.Calculator;
import test.mel.oop.Dog;
import test.mel.oop.Validator;

/**
 * Revision types
 *
 */
public class App {
    public static void main(String[] args) {
        // manipArray();
        // manipNumbers();
        // dogTalk();
        // calculate();
        // evenOdd();
        // isCorrectLength();
        // haveTrailingSpaces();
        // haveSpecialChars();
        // validate();
        // bonus();
    }

    public static void bonus() {
        Bonus bonus = new Bonus();
        System.out.println(bonus.isCorrectPassword("mbRgh7t"));
        System.out.println(bonus.isCorrectEmail("mgalvan@outlook.fr"));
    }

    /**
     * Appel des méthodes de la classe Validator.
     */
    public static void validate() {
        Validator validator = new Validator(2, 32);
        // validator.min = 2;
        // validator.max = 32;
        System.out.println(validator.validate("bl_oup")); // false
    }

    public static void haveSpecialChars() {
        Validator validator = new Validator(2, 32);
        System.out.println(validator.haveSpecialChars("blpo")); // false
    }

    public static void haveTrailingSpaces() {
        Validator validator = new Validator(2, 32);
        System.out.println(validator.haveTrailingSpaces("bloup")); // false
    }

    public static void isCorrectLength() {
        Validator validator = new Validator(2, 32);
        // validator.min = 2;
        // validator.max = 32;
        System.out.println(validator.isCorrectLength("eee")); // true
    }

    /**
     * Appel des méthodes de la classe Calculator
     */
    public static void evenOdd() {
        Calculator casio = new Calculator();
        System.out.println(casio.isEven(2));
    }

    public static void calculate() {
        Calculator casio = new Calculator();
        int addition = casio.add(5, 2);
        int soustract = casio.soustract(10, 2);
        int multiply = casio.multiply(2, 6);
        int divide = casio.divide(5, 3);
        int modulo = casio.modulo(6, 3);

        System.out.println(addition + "\n" + soustract + "\n" + multiply + "\n" + divide + "\n" + modulo + "\n");

        try {
            System.out.println(casio.calculate(6, 2, '+'));
            System.out.println(casio.calculate(8, 4, '-'));
            System.out.println(casio.calculate(5, 10, '*'));
            System.out.println(casio.calculate(4, 2, '/'));
            System.out.println(casio.calculate(5, 3, '%'));
        } catch (Error e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Pour assigner des valeurs aux propriétés de la classe Dog et les renvoyer
     * sous forme de phrase.
     */
    public static void dogTalk() {
        Dog goodDog = new Dog("Fifi", 4, "Berger malinois");
        // goodDog.name = "Fifi";
        // goodDog.age = 4;
        // goodDog.breed = "berger malinois";

        Dog badDog = new Dog();
        // badDog.name = "Fido";
        // badDog.age = 2;
        // badDog.breed = "Corgi";

        goodDog.greeting();
        badDog.greeting();
        badDog.bark(goodDog.name);
    }

    /**
     * Manipuler un tableau et ses valeurs.
     */
    public static void manipArray() {

        // Array of colors
        String[] colors = {
                "bleu", "rouge", "jaune", "vert"
        };

        // init myCat
        String myCat = "rouge";

        // myCat equals index 1 of colors array
        boolean isRed = myCat.equals(colors[1]);
        System.out.println(isRed);

        // init char e
        char e = 'e';
        // init eChar to convert e to string
        String eChar = String.valueOf(e);
        // if colors index 1 containes same value of eChar (e)
        if (colors[1].contains(eChar)) {
            System.out.println("Yes !");
        } else {
            System.out.println("No !");
        }

        // my cat is ... in loop
        for (String color : colors) {
            System.out.println("mon chat est " + color);
        }
    }

    /**
     * Pour manipuler des nombres entiers et à virgule.
     */
    public static void manipNumbers() {
        float price = 1.50f;
        double bigPrice = 345.678;
        byte age = 60;
        int popFrance = 70000000;
        short veryCold = -15000;

        System.out.println("float:" + price + " | double:" + bigPrice + " | byte:" + age + " | int:" + popFrance
                + " | short:" + veryCold);
    }
}
